package io.jenkins.plugins;

import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebResponse;
import org.hamcrest.collection.IsIn;
import org.junit.Rule;
import org.junit.Test;
import org.jvnet.hudson.test.JenkinsRule;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.Collection;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class TestActionTemplate {
    @Rule
    public JenkinsRule j = new JenkinsRule();

    @Test
    public void config() throws IOException, SAXException {
        JenkinsRule.WebClient client = j.createWebClient();

        Page page = client.goTo("template/config", null);
        WebResponse response = page.getWebResponse();

        assertNotNull(response.getContentAsString());

        Collection<String> actions = j.jenkins.getUnprotectedRootActions();
        assertThat(new TemplateAction().getDisplayName(), IsIn.isIn(actions));
    }

}
