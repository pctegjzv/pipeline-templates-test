package io.jenkins.plugins.sample;

import io.alauda.devops.client.AlaudaDevOpsClient;
import io.alauda.devops.client.AlaudaDevOpsConfig;
import io.alauda.devops.client.AlaudaDevOpsConfigBuilder;
import io.alauda.devops.client.DefaultAlaudaDevOpsClient;
import io.alauda.kubernetes.api.model.ObjectMeta;
import io.alauda.kubernetes.api.model.PipelineTaskTemplate;
import io.alauda.kubernetes.api.model.PipelineTemplate;
import io.alauda.kubernetes.api.model.Project;
import org.junit.rules.MethodRule;
import org.junit.runner.Description;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;
import org.jvnet.hudson.test.WithoutJenkins;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;

public class ResourceRule implements MethodRule {
    private final AlaudaDevOpsClient client;
    private Project project;
    private final String projectYaml;

    public ResourceRule(String projectYaml) {
        AlaudaDevOpsConfigBuilder configBuilder = new AlaudaDevOpsConfigBuilder();
        AlaudaDevOpsConfig config = configBuilder.build();
        client =  new DefaultAlaudaDevOpsClient(config);
        this.projectYaml = projectYaml;
    }

    @Override
    public Statement apply(Statement base, FrameworkMethod method, Object target) {
        Description description = Description.createTestDescription(method.getMethod().getDeclaringClass(), method.getName(), method.getAnnotations());
        if (description.getAnnotation(WithoutJenkins.class) != null) {
            return base;
        }

        return new Statement(){

            @Override
            public void evaluate() throws Throwable {
                try {
                    project = loadProject(projectYaml);

                    base.evaluate();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(project != null) {
                        client.projects().delete(project);
                    }
                }
            }
        };
    }

    @CheckForNull
    public PipelineTemplate getPipelineTemplate(@Nonnull InputStream stream) {
        PipelineTemplate template = client.pipelinetemplates().load(stream).get();
        return template;
    }

    public PipelineTemplate loadPipelineTemplate(final String ns, final String fileName) throws IOException {
        try(InputStream stream = getStream(fileName)) {
            if(stream != null) {
                PipelineTemplate template = getPipelineTemplate(stream);
                ObjectMeta metadata = template.getMetadata();
                if(metadata != null) {
                    metadata.setNamespace(ns);
                    metadata.setGenerateName("template-test-");
                } else {
                    return null;
                }

                return client.pipelinetemplates().create(template);
            }
        }

        return null;
    }

    @CheckForNull
    public PipelineTaskTemplate getPipelineTaskTemplate(@Nonnull InputStream stream) {
        PipelineTaskTemplate template = client.pipelinetasktemplates().load(stream).get();
        return template;
    }

    public PipelineTaskTemplate loadPipelineTaskTemplate(final String ns, final String fileName) throws IOException {
        try(InputStream stream = getStream(fileName)) {
            if(stream != null) {
                PipelineTaskTemplate template = getPipelineTaskTemplate(stream);
                ObjectMeta metadata = template.getMetadata();
                if(metadata != null) {
                    metadata.setNamespace(ns);
                    metadata.setGenerateName("template-task-test-");
                } else {
                    return null;
                }

                return client.pipelinetasktemplates().create(template);
            }
        }

        return null;
    }

    public void loadPipelineTaskTemplates(final String ns, final String ...fileNames) throws IOException {
        if(fileNames == null) {
            return;
        }

        for(String fileName : fileNames) {
            loadPipelineTaskTemplate(ns, fileName);
        }
    }

    @CheckForNull
    public Project getProject(@Nonnull InputStream stream) {
        Project project = client.projects().load(stream).get();
        return project;
    }

    public Project loadProject(String fileName) throws IOException {
        try(InputStream stream = getStream(fileName)) {
            if(stream != null) {
                Project project = getProject(stream);
                project.getMetadata().setGenerateName("project-test-");
                return client.projects().create(project);
            }
        }

        return null;
    }

    @CheckForNull
    public InputStream getStream(@Nonnull String fileName) {
        ClassLoader loader = this.getClass().getClassLoader();
        return loader.getResourceAsStream(fileName);
    }

    public Project getProject() {

        return project;
    }

    public AlaudaDevOpsClient getClient() {
        return client;
    }
}
