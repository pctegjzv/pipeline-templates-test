package io.jenkins.plugins.sample;

import io.alauda.devops.client.AlaudaDevOpsConfig;
import io.alauda.devops.client.AlaudaDevOpsConfigBuilder;
import io.alauda.devops.client.DefaultAlaudaDevOpsClient;
import io.alauda.jenkins.devops.sync.util.CredentialsUtils;
import io.alauda.kubernetes.api.model.Secret;
import io.jenkins.plugins.TemplateAction;
import io.jenkins.plugins.casc.ConfigurationAsCode;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.webapp.Configuration;
import org.eclipse.jetty.webapp.WebAppContext;
import org.eclipse.jetty.webapp.WebXmlConfiguration;
import org.jvnet.hudson.test.JenkinsRule;
import org.jvnet.hudson.test.NoListenerConfiguration;
import org.jvnet.hudson.test.ThreadPoolImpl;
import org.jvnet.hudson.test.WarExploder;

import javax.servlet.ServletContext;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import static io.jenkins.plugins.casc.ConfigurationAsCode.CASC_JENKINS_CONFIG_PROPERTY;

public class AlaudaJenkinsRule extends JenkinsRule {
    private static final Logger LOGGER = Logger.getLogger(AlaudaJenkinsRule.class.getName());

    private String dockerCredentialId;

    @Override
    public void before() throws Throwable {
        super.before();

        setupConfig();

        setupCredential();
    }

    private void setupConfig() throws IOException {
        String config = new URL(getURL(), new TemplateAction().getUrlName() + "/config").toString();
        System.setProperty(CASC_JENKINS_CONFIG_PROPERTY, config);
        ConfigurationAsCode.get().configure();
        System.clearProperty(CASC_JENKINS_CONFIG_PROPERTY);
    }

    private void setupCredential() throws IOException {
        AlaudaDevOpsConfigBuilder configBuilder = new AlaudaDevOpsConfigBuilder();
        AlaudaDevOpsConfig config = configBuilder.build();
        DefaultAlaudaDevOpsClient client = new DefaultAlaudaDevOpsClient(config);

        final String ns = "alauda-system";
        final String secretName = "docker-for-test";
        Secret secret = client.secrets().inNamespace(ns).withName(secretName).get();
        if(secret != null) {
            dockerCredentialId = CredentialsUtils.upsertCredential(secret, ns, secretName);
        }
    }

    @Override
    protected ServletContext createWebServer() throws Exception {
        this.server = new Server(new ThreadPoolImpl(new ThreadPoolExecutor(10, 10, 10L, TimeUnit.SECONDS, new LinkedBlockingQueue(), new ThreadFactory() {
            public Thread newThread(Runnable r) {
                Thread t = new Thread(r);
                t.setName("Jetty Thread Pool");
                return t;
            }
        })));
        WebAppContext context = new WebAppContext(WarExploder.getExplodedDir().getPath(), this.contextPath);
        context.setClassLoader(this.getClass().getClassLoader());
        context.setConfigurations(new Configuration[]{new WebXmlConfiguration()});
        context.addBean(new NoListenerConfiguration(context));
        this.server.setHandler(context);
        context.setMimeTypes(MIME_TYPES);
        context.getSecurityHandler().setLoginService(this.configureUserRealm());
        context.setResourceBase(WarExploder.getExplodedDir().getPath());
        ServerConnector connector = new ServerConnector(this.server, 1, 1);
        HttpConfiguration config = ((HttpConnectionFactory)connector.getConnectionFactory(HttpConnectionFactory.class)).getHttpConfiguration();
        config.setRequestHeaderSize(12288);
        connector.setHost("0.0.0.0");
        if (System.getProperty("port") != null) {
            connector.setPort(Integer.parseInt(System.getProperty("port")));
        }

        this.server.addConnector(connector);
        this.server.start();
        this.localPort = connector.getLocalPort();
        LOGGER.log(Level.INFO, "Running on {0}", this.getURL());
        return context.getServletContext();
    }

    public String getDockerCredentialId() {
        return dockerCredentialId;
    }
}
