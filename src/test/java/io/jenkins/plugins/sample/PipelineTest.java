package io.jenkins.plugins.sample;

import io.alauda.kubernetes.api.model.*;
import io.alauda.kubernetes.client.KubernetesClientException;
import org.jenkinsci.plugins.workflow.cps.CpsFlowDefinition;
import org.jenkinsci.plugins.workflow.flow.FlowDefinition;
import org.jenkinsci.plugins.workflow.job.WorkflowJob;
import org.junit.Rule;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class PipelineTest {
    static {
        // since kubernetes plugin will spend much time
        System.setProperty("jenkins.test.timeout", "1200");
    }

    @Rule
    public AlaudaJenkinsRule j = new AlaudaJenkinsRule();
    @Rule
    public ResourceRule res = new ResourceRule("project.yaml");

    @Test
    public void javaBuild() throws Exception {
        final String credentialId = j.getDockerCredentialId();
        assertNotNull("can't found docker credential.", credentialId);

        Map<String, String> values = new HashMap<>();
        values.put("RepositoryPath", "https://github.com/surenpi/docker-sample");
        values.put("registry", "index.alauda.cn/alaudaorg/");
        values.put("repository", "test");
        values.put("credentialsId", credentialId);

        String jenkinsfile = null;
        try {
            jenkinsfile = generateJenkinsfile("template-java-build.yaml",
                    values,
                    "task-clone.yaml",
                    "task-build-docker.yaml",
                    "task-build-java.yaml");
        } catch (KubernetesClientException e) {
            fail("can not create resources correctly.");
            e.printStackTrace();
        }
        System.out.println(jenkinsfile);

        FlowDefinition definition = new CpsFlowDefinition(jenkinsfile, true);

        WorkflowJob wf = j.createProject(WorkflowJob.class);
        wf.setDefinition(definition);

        j.buildAndAssertSuccess(wf);
    }

    @Test
    public void golangBuild() throws Exception {
        final String credentialId = j.getDockerCredentialId();
        assertNotNull("can't found docker credential.", credentialId);

        Map<String, String> values = new HashMap<>();
        values.put("RepositoryPath", "https://github.com/surenpi/golang-http-sample");
        values.put("RelativeDirectory", "src/github.com/surenpi");
        values.put("registry", "index.alauda.cn/alaudaorg/");
        values.put("repository", "test");
        values.put("credentialsId", credentialId);

        final String jenkinsfile = generateJenkinsfile("template-golang-build.yaml",
                values,
                "task-clone.yaml",
                "task-build-docker.yaml",
                "task-build-golang.yaml");
        System.out.println(jenkinsfile);
        assertNotNull("generate jenkinsfile failed.", jenkinsfile);

        FlowDefinition definition = new CpsFlowDefinition(jenkinsfile, true);

        WorkflowJob wf = j.createProject(WorkflowJob.class);
        wf.setDefinition(definition);

        j.buildAndAssertSuccess(wf);
    }

    private PipelineTemplate loadResources(String template, String ...tasks) throws IOException {
        final Project project = res.getProject();
        final String ns = project.getMetadata().getName();

        res.loadPipelineTaskTemplates(ns, tasks);

        return res.loadPipelineTemplate(ns, template);
    }

    private String generateJenkinsfile(String template, Map<String, String> values, String ...tasks) throws IOException {
        final Project project = res.getProject();
        final String ns = project.getMetadata().getName();

        PipelineTemplate buildTemplate = loadResources(template, tasks);

        JenkinsfilePreviewOptions options = new JenkinsfilePreviewOptionsBuilder()
                .withValues(values)
                .build();
        JenkinsfilePreview preview = res.getClient().pipelinetemplates()
                .inNamespace(ns)
                .withName(buildTemplate.getMetadata().getName())
                .preview(options);

        assertNotNull(preview);

        final String jenkinsfile = preview.getJenkinsfile();
        assertNotNull(jenkinsfile);

        return jenkinsfile.replace("deleteDir()", ""); // TODO just a temp solution
    }
}
