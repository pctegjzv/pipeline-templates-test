package io.jenkins.plugins.sample;

public interface LoadResources {
    void load(String fileName);
}
