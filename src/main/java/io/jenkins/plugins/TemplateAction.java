package io.jenkins.plugins;

import hudson.Extension;
import hudson.model.UnprotectedRootAction;
import hudson.util.HttpResponses;
import jenkins.model.Jenkins;
import org.jenkinsci.Symbol;
import org.kohsuke.stapler.HttpResponse;
import org.kohsuke.stapler.export.ExportedBean;

import javax.annotation.CheckForNull;
import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.util.Enumeration;
import java.util.List;

@Extension
@Symbol("template")
@ExportedBean
public class TemplateAction implements UnprotectedRootAction {
    @CheckForNull
    @Override
    public String getIconFileName() {
        return null;
    }

    @CheckForNull
    @Override
    public String getDisplayName() {
        return "Alauda Pipeline Template Test";
    }

    @CheckForNull
    @Override
    public String getUrlName() {
        return "template";
    }

    public HttpResponse doConfig() throws IOException {
        StringBuffer buffer = new StringBuffer();
        ClassLoader loader = this.getClass().getClassLoader();
        try(InputStream stream = loader.getResourceAsStream("jenkins.yaml")) {
            byte[] buf = new byte[1024];
            int len = -1;
            while((len = stream.read(buf)) != -1) {
                buffer.append(new String(buf, 0 , len));
            }
        }

        String yaml = buffer.toString();
        Jenkins jenkins = Jenkins.get();
        final String address = getAddress();
        final String rootUrlStr = jenkins.getRootUrl();
        if(rootUrlStr != null) {
            URL rootUrl = new URL(rootUrlStr);
            yaml = yaml.replace("${jenkinsUrl}", "http://" + address + ":" + rootUrl.getPort() + rootUrl.getPath());
            yaml = yaml.replace("${jenkinsTunnel}",
                    address + ":" + jenkins.getTcpSlaveAgentListener().getPort());
        }

        return HttpResponses.text(yaml);
    }

    private String getAddress() {
        try {
            Enumeration netInterfaces = NetworkInterface.getNetworkInterfaces();
            InetAddress ip = null;
            while (netInterfaces.hasMoreElements()) {
                NetworkInterface ni = (NetworkInterface) netInterfaces.nextElement();

                List<InterfaceAddress> sf = ni.getInterfaceAddresses();
                for(InterfaceAddress it : sf)
                {
                    ip = it.getAddress();
                    if(ip.isLoopbackAddress() || ip.isLinkLocalAddress()){
                        continue;
                    }
                    return ip.getHostAddress();
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }

        return "127.0.0.1";
    }
}
