FROM alpine:3.7

COPY target/pipeline-templates-test.hpi /plugins/
COPY src/test/resources/*.yaml /templates/